<?php
namespace mathewparet\ModelSuspension;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Schema\Blueprint;


class SuspensionServiceProvider extends ServiceProvider
{
    /**
     * Register service
     */
    public function register(): void
    {
        // 
    }

    /**
     * Bootstrap services
     */
    public function boot(): void
    {
        $this->defineSuspensionBlueprint();

        $this->defineSuspensionRoutes();
    }

    private function defineSuspensionRoutes()
    {
        Route::macro('suspensionRoutes', function($model, $controller) {
            $model_plural = Str::of($model)->plural();

            $model_singular = Str::of($model)->singular();
            
            $route_prefix = __(':model_plural/{:model_singular}', [
                'model_plural' => $model_plural,
                'model_singular' => $model_singular,
            ]);

            foreach(['suspend', 'unsuspend'] as $method)
            {
                $route_name = __(':model_plural.:name', [
                    'model_plural' => $model_plural, 
                    'name' => $method
                ]);

                Route::patch($route_prefix.'/'.$method, [$controller, $method])->name($route_name);
            }
        });
    }

    private function defineSuspensionBlueprint()
    {
        Blueprint::macro("canBeSuspended", function($name = 'suspended_at') {
            /**
             * @var \Illuminate\Database\Schema\Blueprint $this
             */
            return $this->dateTime($name)->nullable();
        });
    }
}

