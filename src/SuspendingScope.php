<?php
namespace mathewparet\ModelSuspension;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Contracts\Database\Eloquent\Builder;

class SuspendingScope implements Scope
{
    /**
     * All of the extensions to be added to the builder.
     *
     * @var string[]
     */
    protected $extensions = ['Unsuspend', 'WithSuspended', 'WithoutSuspended', 'OnlySuspended'];

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $builder->whereNull($model->getQualifiedSuspendedAtColumn());
    }

    /**
     * Extend the query builder with the needed functions.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    public function extend(Builder $builder)
    {
        foreach ($this->extensions as $extension) {
            $this->{"add{$extension}"}($builder);
        }
    }

    /**
     * Get the "suspended at" column for the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return string
     */
    protected function getSuspendedAtColumn(Builder $builder)
    {
        if (count((array) $builder->getQuery()->joins) > 0) {
            return $builder->getModel()->getQualifiedSuspendedAtColumn();
        }

        return $builder->getModel()->getSuspendedAtColumn();
    }

    /**
     * Add the unsuspend extension to the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    protected function addUnsuspend(Builder $builder)
    {
        $builder->macro('unsuspend', function (Builder $builder) {
            $builder->withSuspended();

            return $builder->update([$builder->getModel()->getSuspendedAtColumn() => null]);
        });
    }

    /**
     * Add the with-suspended extension to the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    protected function addwithSuspended(Builder $builder)
    {
        $builder->macro('withSuspended', function (Builder $builder, $withSuspended = true) {
            if (! $withSuspended) {
                return $builder->withoutSuspended();
            }

            return $builder->withoutGlobalScope($this);
        });
    }

    /**
     * Add the without-suspended extension to the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    protected function addwithoutSuspended(Builder $builder)
    {
        $builder->macro('withoutSuspended', function (Builder $builder) {
            $model = $builder->getModel();

            $builder->withoutGlobalScope($this)->whereNull(
                $model->getQualifiedSuspendedAtColumn()
            );

            return $builder;
        });
    }

    /**
     * Add the only-suspended extension to the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    protected function addOnlySuspended(Builder $builder)
    {
        $builder->macro('onlySuspended', function (Builder $builder) {
            $model = $builder->getModel();

            $builder->withoutGlobalScope($this)->whereNotNull(
                $model->getQualifiedSuspendedAtColumn()
            );

            return $builder;
        });
    }
}