<?php
namespace mathewparet\ModelSuspension;

/**
 * @var string SUSPENDED_AT
 */
trait CanBeSuspended
{
    /**
     * Boot the supension management trait for a model.
     *
     * @return void
     */
    public static function bootCanBeSuspended()
    {
        static::addGlobalScope(new SuspendingScope);
    }

    /**
     * Initialize the supension management trait for an instance.
     *
     * @return void
     */
    public function initializeCanBeSuspended()
    {
        if (! isset($this->casts[$this->getSuspendedAtColumn()])) {
            $this->casts[$this->getSuspendedAtColumn()] = 'datetime';
        }
    }

    /**
     * Suspend a model
     *
     * @return bool|null
     */
    public function suspend()
    {
        if ($this->fireModelEvent('suspending') === false) {
            return false;
        }
        return tap($this->runSuspension(), function ($suspended) {
            if ($suspended) {
                $this->fireModelEvent('suspended', false);
            }
        });
    }

    /**
     * Perform the actual suspension query on this model instance.
     *
     * @return mixed
     */
    protected function performSuspendOnModel()
    {
        return $this->runSuspension();
    }

    /**
     * Perform the actual suspension query on this model instance.
     *
     * @return void
     */
    protected function runSuspension()
    {
        $query = $this->setKeysForSaveQuery($this->newModelQuery());

        $time = $this->freshTimestamp();

        $columns = [$this->getSuspendedAtColumn() => $this->fromDateTime($time)];

        $this->{$this->getSuspendedAtColumn()} = $time;

        if ($this->usesTimestamps() && ! is_null($this->getUpdatedAtColumn())) {
            $this->{$this->getUpdatedAtColumn()} = $time;

            $columns[$this->getUpdatedAtColumn()] = $this->fromDateTime($time);
        }

        $query->update($columns);

        $this->syncOriginalAttributes(array_keys($columns));

        $this->fireModelEvent('suspended', false);
    }

    /**
     * Suspend model without raising any events.
     *
     * @return bool|null
     */
    public function suspendQueitly()
    {
        return static::withoutEvents(fn () => $this->runSuspension());
    }

    /**
     * Register a "suspending" model event callback with the dispatcher.
     *
     * @param  \Closure|string  $callback
     * @return void
     */
    public static function suspending($callback)
    {
        static::registerModelEvent('suspending', $callback);
    }

    /**
     * Register a "suspended" model event callback with the dispatcher.
     *
     * @param  \Closure|string  $callback
     * @return void
     */
    public static function suspended($callback)
    {
        static::registerModelEvent('suspended', $callback);
    }

    /**
     * Determine if the model instance has been suspended.
     *
     * @return bool
     */
    public function isSuspended()
    {
        return ! is_null($this->{$this->getSuspendedAtColumn()});
    }

    /**
     * Unsuspend model instance.
     *
     * @return bool
     */
    public function unsuspend()
    {
        // If the unsuspending event does not return false, we will proceed with this
        // unsuspend operation. Otherwise, we bail out so the developer will stop
        // the unsuspend totally. We will clear the suspended timestamp and save.
        if ($this->fireModelEvent('unsuspending') === false) {
            return false;
        }

        $this->{$this->getSuspendedAtColumn()} = null;

        // Once we have saved the model, we will fire the "unsuspended" event so this
        // developer will do anything they need to after a unsuspend operation is
        // totally finished. Then we will return the result of the save call.
        $this->exists = true;

        $result = $this->save();

        $this->fireModelEvent('unsuspended', false);

        return $result;
    }

    /**
     * Unsuspend model instance without raising any events.
     *
     * @return bool
     */
    public function unsuspendQuietly()
    {
        return static::withoutEvents(fn () => $this->unsuspend());
    }

    /**
     * Register a "unsuspending" model event callback with the dispatcher.
     *
     * @param  \Closure|string  $callback
     * @return void
     */
    public static function unsuspending($callback)
    {
        static::registerModelEvent('unsuspending', $callback);
    }

    /**
     * Register a "unsuspended" model event callback with the dispatcher.
     *
     * @param  \Closure|string  $callback
     * @return void
     */
    public static function unsuspended($callback)
    {
        static::registerModelEvent('unsuspended', $callback);
    }

    /**
     * Get the name of the "suspended at" column.
     *
     * @return string
     */
    public function getSuspendedAtColumn()
    {
        return defined(static::class.'::SUSPENDED_AT') ? static::SUSPENDED_AT : 'suspended_at';
    }

    /**
     * Get the fully qualified "suspended at" column.
     *
     * @return string
     */
    public function getQualifiedSuspendedAtColumn()
    {
        return $this->qualifyColumn($this->getSuspendedAtColumn());
    }
}