# mathewparet/model-suspension

Provide the ability to models to be suspended.

## Features

1. Suspend / Unsuspend a model.
2. Suspended models are not considered for any queries by default (adds a global scope).

# Installation

```shell
composer require mathewparet/model-suspension
```

# Defining a field to hold suspension status in migrations

```php
// define a 'suspended_at' field of type dateTime and is nullable
$table->canBeSuspended();
```

or


```php
// define a 'flagged_at' field of type dateTime and is nullable
$table->canBeSuspended('flagged_at');
```

# Add the funtionality to your model

```php
// ...
use mathewparet\ModelSuspension\CanBeSuspended;
// ...
class User extends Authenticatable
{
    // ...
    use CanBeSuspended;
    // ...

    /**
     * To use custom field name for suspension, use the below line. If the below line isn't 
     * defined, the field is assumed to be 'suspended_at'.
     */
    const SUSPENDED_AT = 'flagged_at';
}
```

# API referance

This package introduces the below methods:

| Name | Availability | Description |
|---|---|---|
`canBeSuspended()`|Blueprint|Macro to define a field that holds the suspension state for the record.|
|`suspend()`|Model|Mark a model as suspended.|
|`suspendQuetly()`|Model|Mark a model as suspended without raising any events.|
|`unsuspend()`|Model, Builder|Mark a model as unsuspended.|
|`unsuspendQuetly()`|Model|Mark a model as unsuspended without raising any events.|
|`withSuspended()`|Model|Add suspended records to the scope.|
|`withoutSuspended()`|Model|Remove suspended records from scope.|
|`onlySuspended()`|Model|Scope the query to only consider suspended records.|


# Events

1. `suspending`
2. `suspneded`
3. `unsuspending`
4. `unsuspended`